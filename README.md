# ones

#### 介绍
整合分布式开发平台
基于现实中服务模块的拆分设计，构建快速扩展的响应式业务

#### 软件架构
按照一个分布式集群的架构进行整体设计及拆分，要求是可插拔，快速切换，易于集成，高性能的实现
* 拆分模块：
    - parent：超级父类设计，基于规范要求
    - core：核心的功能组合包，顶层是pom，可以根据网络，事物，存储等实现不同的core，业务或者功能侧根据需要自由组合
    - common：公用的模块设计，每个系统会有常用的及共享的变量及通用功能，内部拆分，根据需要组合
    - service：业务侧的模块，根据业务分类，实现独立的业务功能，只是简单的业务功能
    - server：服务侧模块，拆分服务和功能，服务提供对应协议的容器，功能是自由组合
    - boots：基于springboot的快速集成，提供给server的选择实现
    - plugins：插件性能，基于接口的设计，如果业务侧有特殊要求，可以根据规范实现接口即可
    - interface：接口模块，主要设计为暴露外部使用，供服务间的快速调用及协议操作
    - utils：工具模块，提供统一的工具类分类操作

#### 使用说明

1. 整体设计规范
* 要求快速实现，编码规范，性能要求
2. 服务拆分依据
3. 拆分关系规范
4. 整体蓝图

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)